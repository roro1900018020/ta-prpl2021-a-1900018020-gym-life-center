<?php 
 
    // Mmemanggil koneksi databas 
    include '../../koneksi.php';

    if (isset($_POST['submit_data_pelanggan'])) {
        $nama           = $_POST['nama'];
        $pelatihan      = $_POST['pelatihan'];
        $instruktur     = $_POST['instruktur'];
        $hari           = $_POST['hari'];
        $waktu          = $_POST['waktu'];
        $alamat         = $_POST['alamat'];
        $no_telp        = $_POST['no_telp'];

        $sql_insert= "INSERT INTO pelanggan VALUES ('$nama', '$pelatihan', '$instruktur', $hari', $waktu', '$alamat', '$no_telp')";
        $result = mysqli_query($koneksi, $sql_insert);

        echo "<script>window.alert('Data anda berhasil di input!!'); window.location = 'pelanggan.php?id_paket=$id_paket';</script>";
        header("Location:transaksi.php");
    }

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>WELCOME TO RORO</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link rel="stylesheet" href="../dist/css/skins/skin-blue.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="../https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="index.php" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>GLC</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>GYM LIFE CENTER</b></span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
        </nav>
    </header>

    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                
                <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                <li><a href="#"><i class="fa fa-user"></i> <span>Member</span></a></li>
                <li><a href="transaksi.php"><i class="fa fa-list"></i> <span>Data Transaksi</span></a></li>
                <li><a href="logout.php"><i class="glyphicon glyphicon-off"></i> <span>Logout</span></a></li>
            </ul>
          <!-- /.sidebar-menu -->
        </section>
    <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">

            <div class="box box-primary">

                <div class="box-body">

                    <div class="alert alert-info alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-info"></i> ISI FORM MEMBER!</h4>
                        Mohon di isi dengan lengkap.
                    </div>

                    <!-- enctype="multipart/form-data" adalah sebuah function untuk upload gambar -->
                    <form action="" method="POST" role="form">

                        <div class="form-group">
                            <label for="nama">Nama Member</label>
                            <input type="text" class="form-control" id="nama" name="nama" <?php if (!isset($_POST['submit_data_pelanggan'])) { echo "placeholder='Cth: roro'"; }elseif (isset($_POST['submit_data_pelanggan'])) { echo  "readonly"; } ?> required>
                        </div>

                        <div class="form-group">
                            <label for="pelatihan">Jenis Pelatihan</label>
                            <input type="text" class="form-control" id="pelatihan" name="pelatihan" <?php if (!isset($_POST['submit_data_pelanggan'])) { echo "placeholder='Cth: latihan otot dada'"; }elseif (isset($_POST['submit_data_pelanggan'])) { echo  "readonly"; } ?> required>
                        </div>

                        <div class="form-group">
                            <label for="instruktur">Instruktur</label>
                            <input type="text" class="form-control" id="instruktur" name="instruktur" <?php if (!isset($_POST['submit_data_pelanggan'])) { echo "placeholder='Cth: Radita Aryani'"; }elseif (isset($_POST['submit_data_pelanggan'])) { echo "value='".$instruktur."' readonly"; } ?> required>
                        </div>

                        <div class="form-group">
                            <label for="instruktur">Hari</label>
                            <input type="text" class="form-control" id="hari" name="hari" <?php if (!isset($_POST['submit_data_pelanggan'])) { echo "placeholder='Cth: selasa'"; }elseif (isset($_POST['submit_data_pelanggan'])) { echo "value='".$hari."' readonly"; } ?> required>
                        </div>

                        <div class="form-group">
                            <label for="instruktur">Waktu</label>
                            <input type="text" class="form-control" id="waktu" name="jam" <?php if (!isset($_POST['submit_data_pelanggan'])) { echo "placeholder='Cth: '"; }elseif (isset($_POST['submit_data_pelanggan'])) { echo "value='".$waktu."' readonly"; } ?> required>
                        </div>

                        <div class="form-group">
                            <label for="alamat">Alamat</label>
                            <input type="text" class="form-control" id="alamat" name="alamat" <?php if (!isset($_POST['submit_data_pelanggan'])) { echo "placeholder='Cth: Jogja'"; }elseif (isset($_POST['submit_data_pelanggan'])) { echo "value='".$alamat."' readonly"; } ?> required>
                        </div>

                        <div class="form-group">
                            <label for="no_telp">No. Telp.</label>
                            <input type="text" class="form-control" id="no_telp" name="no_telp" <?php if (!isset($_POST['submit_data_pelanggan'])) { echo "placeholder='Cth: 085363430844'"; }elseif (isset($_POST['submit_data_pelanggan'])) { echo "value='".$no_telp."' readonly"; } ?> required>
                        </div>

                        <?php if (!isset($_POST['submit_data_pelanggan'])) { ?>
                        <button type="submit"name="submit_data_pelanggan"class="btn btn-success">Selesai</button>
                        <?php } ?>
                    </form>
                </div>
            </div>

            <?php if (isset($_POST['submit_data_pelanggan'])) { ?>

            <?php } ?>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
            Anything you want
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2021 <a href="#">Website</a>.</strong> GYM LIFE CENTER.
    </footer>

</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->
<script src="../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../bootstrap/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/app.min.js"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
</body>
</html>